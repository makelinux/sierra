# sierra

A library of Sierra, Legato, mangOH, Yocto tools

Features:

* automated
* can be unattended and suitable for Jenkins or another batch build
* runs on fresh OS
* downloads and installs required OS packages
* installs sierra tools: poky-swi-ext toolchain, legato_spm, swicwe
* builds Legato, mangOH, Yocto
* works on Ubuntu 16.04 and 18.04
* tested on wp76xx green, expandable for more boards
* uses docker for Yocto build if needed

The script automates build process described in docs:
* https://docs.legato.io/latest/basicBuild.html
* https://github.com/mangOH/mangOH
* https://github.com/legatoproject/legato-af/blob/master/README.md

Usage:

One automatic step:
<pre>
 ./sierra-tools.sh all
</pre>
Manual steps:
<pre>
 . ./sierra-tools.sh all
 config-env
 prepare
 make -C $MANGOH_ROOT ${board}_$target
 make -C yocto
</pre>
