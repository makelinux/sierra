#!/bin/bash

# source this script
# or run with argument "all"

# References:
# https://docs.legato.io/latest/basicBuild.html
# https://github.com/mangOH/mangOH
# https://github.com/legatoproject/legato-af/blob/master/README.md

showv()
{
	local a
	for a in "$@"; do
		echo $a=${!a};
	done
}

setv()
{
	local var=$1
	shift
	local val="$@"
	eval $var=\""$val"\"
	echo $var=\""$val"\"
}

symlink()
{
	ln --force --no-dereference --relative --symbolic --verbose "$@"
}

config-env()
{
	[ "$target" ] || target=wp76xx
	[ "$board" ] || board=green
	[ "$wp" ] || wp=$target
	# release=release-12
	[ "$release" ] || release=latest-release
	sierra_release=$(expr match "$(cat $release-components.list)" ".*Release\([0-9.]*\)")
	sierra_major=${sierra_release%%.*}
	sierra=$PWD
	target_ip=192.168.2.2
	fw="$sierra/yocto/build_bin/tmp/deploy/images/swi-mdm9x28*/yocto_$target.4k.cwe"
	mkdir -p $sierra_major
	toolchain=$PWD/$sierra_major/poky-swi-ext-toolchain
	nproc=$(getconf _NPROCESSORS_ONLN)
	export ${wp^^}_TOOLCHAIN_DIR=$toolchain/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi
	export ${wp^^}_TOOLCHAIN_PREFIX=arm-poky-linux-gnueabi-
	export CROSS_COMPILE=arm-poky-linux-gnueabi-
	expr match "$PATH" "$WP76XX_TOOLCHAIN_DIR" || PATH+=:$WP76XX_TOOLCHAIN_DIR;
	export LEGATO_ROOT=$PWD/legato
	export MANGOH_ROOT=$PWD/mangOH
	export LEGATO_SYSROOT=$toolchain/sysroots/armv7a-neon-poky-linux-gnueabi
	declare -gA mango_hash
	mango_hash[10.1]=b90edecfafe441728e5e247a1cf7ad179b039487
	mango_hash[12]=master
	storage=(. ~/Downloads/ $PWD/yocto/downloads /mnt)
	install=(apt-utils build-essential openjdk-8-jdk
		python python3 python-{jinja2,pkg-resources,git}
		cmake git diffstat texinfo gawk chrpath
		wget atool jq
		cpio bash ninja-build bc unzip libxml2-utils gcovr
		{libcurl4-gnutls,zlib1g,libbz2,libssl,libsdl1.2}-dev
		bison flex gperf
		bsdiff autoconf automake iputils-ping)
}

download() # cached download
{
	local b=$(basename "$1")
	local fn=$(find -L ${storage[*]} -maxdepth 6 -name "$b" -print -quit 2> /dev/null)
	test -e "$fn" && { echo "$fn"; return; }
	wget -P ~/Downloads "$1" || return
	echo ~/Downloads/"$b"
}

tarbasename()
{
	expr match "$1" '.*/\(.*\)\.tar' \| match "$1" '.*/\(.*\)\.' \| match "$1" '.*/\(.*\)'
}

unpack()
{
	if f=$(download $1); then
		b="$(tarbasename "$f")"
		(1>&2 echo unpacking $b )
		mkdir -p "$b"
		tar --strip-components=1 --directory="$b" --extract --auto-compress --file="$f"
		echo "$b"
	fi
}

legato-from-repo()
{
	test -e $LEGATO_ROOT || tar xaf $(download legato-repo.tgz)
	if [ ! -e $LEGATO_ROOT ]; then
		echo legato ...
		pushd $(dirname $LEGATO_ROOT)
		repo init --no-repo-verify -u https://github.com/legatoproject/manifest -m legato/releases/$legato_release.xml
		repo sync --no-repo-verify
		popd
	fi
	if ! find legato/synced -mtime -1 -print; then
		git -C .repo/manifests clean -d --force
		git -C .repo/repo clean -d --force
		repo sync
		repo init https://github.com/legatoproject/manifest -m legato/releases/$legato_release.xml
		repo sync
		touch legato/synced
		git -C legato checkout ${legato_release%.*}-release
	fi
}

prepare()
{
	if [ -t 1 ]; then # in terminal only, don't run from pipeline
		#sudo --non-interactive apt-get update
		echo Checking installed packages
		(set +x;
		for m in ${install[*]}; do
			echo -en "$m\e[K\r"
			dpkg -V $m 2> /dev/null || sudo apt-get install -y -q $m &> apt-get-install.log
			echo -en "\e[K"
		done)
	fi
	which repo > /dev/null || { sudo wget -q -O /usr/local/bin/repo https://storage.googleapis.com/git-repo-downloads/repo; sudo chmod a+x /usr/local/bin/repo; }
	which swicwe
	which swicwe > /dev/null || { sudo dpkg -i $(download http://downloads.sierrawireless.com/tools/swicwe/swicwe_latest.deb); }
	which swiflash > /dev/null || { sudo dpkg -i $(download https://downloads.sierrawireless.com/tools/swiflash/swiflash_latest.deb); }
	which leaf > /dev/null || sudo apt-get install -y $(download https://downloads.sierrawireless.com/tools/leaf/leaf_latest.deb)
	local j=/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
	test $(readlink /etc/alternatives/java) == $j || sudo update-alternatives --set java $j
	# sudo apt autoremove -y
	local sw=https://source.sierrawireless.com/resources/airprime/software
	test -e legato_spm || aunpack --extract-to=legato_spm $(download $(wget -O- $sw/legato_spm/ | grep -o 'http://.*/legato-spm.*\.zip'))
	# TODO: support offline
	local components
	find $release-components.list -mtime -1 || \
		components="$(wget --timeout=10 -O- $sw/$wp/$wp-firmware-${release}-components/ | grep -o 'https\?://d[^"]*')"
	test "$components" && echo "$_" > $release-components.list || components="$(cat $release-components.list)"
	showv components
	setv sierra_release $(expr match "$components" ".*Release\([0-9.]*\)")
	setv legato_release $(expr match "$components" ".*legato-\([0-9.]*.[0-9]\)")
	which ${CROSS_COMPILE}gcc || config-env # update $toolchain
	if [ ! -e $toolchain ]; then
		toolchain_dl=$(echo "$components" \
			| grep -o 'http.*x86_64.*toolchain.*sh' | tail -n 1)
		if toolchain_i=$(download $toolchain_dl); then
			chmod +x $toolchain_i
			bash $toolchain_i -n -y -d $toolchain
		fi
	fi

	# first try to unpack saved tgz
	# legato-from-repo
	# still don't exist, fallback to tarball
	if [ ! -e $LEGATO_ROOT ]; then
		local legato="$(unpack $(echo "$components" | grep -o 'http.*legato.*' | tail -n1))"
		test "$legato" && symlink $legato $LEGATO_ROOT
	fi
	test -e $MANGOH_ROOT || unpack mangOH.tgz
	test -e $MANGOH_ROOT || {
		echo mangOH ...
		git clone -q --recursive https://github.com/mangOH/mangOH $MANGOH_ROOT
	}
	pushd $MANGOH_ROOT
	git fetch
	echo $sierra_release
	declare -p mango_hash
	git checkout --merge ${mango_hash[$sierra_release]}
	git submodule update --init --recursive
	popd

	test $(readlink -f /bin/sh) == /bin/dash && sudo ln -sfv bash /bin/sh

	local yocto_dl=$(echo "$components" | grep -o 'http.*Legato-Dist-Source.*bz2' | tail -n 1)
	local y=$(tarbasename "$yocto_dl")
	if [ ! -e $y/Makefile ]; then
		yocto_i=$(download $yocto_dl) && unpack $yocto_i
		cat yocto/meta-swi-extras/meta-swi-mdm9x28*/files/fw-version
	fi
	symlink $y yocto
	symlink $LEGATO_ROOT yocto/legato
	git config --get --global user.email > /dev/null || git config --global user.email "$USER@$HOSTNAME"
	git config --get --global user.name > /dev/null || git config --global user.name $USER
	git config --global color.ui auto
}

alias target-update='$sierra/legato/bin/fwupdate download $fw $target_ip'

# ls /dev/serial/by-id/usb-Sierra*if00-port0
# sudo swiflash -m $wp -i $fw

target-status()
{
	ssh root@$target_ip '. /etc/profile; cat /etc/legato/version; fwupdate query; legato version; legato status'
}

_status()
{
	showv sierra_release
	echo Gits:
	find legato/ -maxdepth 4 -name .git -type d -prune -exec sh -c 'cd "{}/.."; pwd; git status --short' \;
	find yocto/ -maxdepth 4 -name .git -type d -prune -exec sh -c 'cd "{}/.."; pwd; git status --short' \;
	echo end of Gits
	tests=(legato/build/$target/tests/apps/*.$target.update); echo Tests legato/build/$target/tests/apps/ : ${#tests[*]}
	samples=(legato/build/$target/samples/*.$target.update); echo Samples legato/build/$target/samples/ : ${#samples[*]}
	for u in legato/build/$target/system.$target.update $MANGOH_ROOT/build/update_files/$board/mangOH.$target.update; do
		ls -l ${u/$PWD\//}
		echo -e $(./legato/bin/update-util $u | jq -r .name) "\n"
	done
	ls -l ${fw/$PWD\//}
	test -e $fw && swicwe -P $fw
	grep --with-filename '' \
		yocto/meta-swi-extras/meta-swi-mdm9x28*/files/fw-version \
		${LEGATO_ROOT/$PWD\//}/build/wp76xx/system/staging/version \
		$toolchain/version*
	echo -n 'legato '; git -C ${LEGATO_ROOT} describe --contains
	echo -n 'linux '; make -s -C yocto/kernel/ kernelversion
}

yocto-build-direct()
{
	#make -C yocto clean
	#NUM_THREADS=$nproc BB_NUMBER_THREADS=$nproc \
	rm -rf yocto/kernel/.config yocto/kernel/include/config
	#export PARALLEL_MAKE="--jobs=$nproc --load-average=$nproc"
	(cd yocto; PATH=/usr/sbin:/usr/bin:/sbin:/bin make) # 'make -C yocto' fails
}

yocto-build()
{
	. /etc/lsb-release
	test -e yocto/poky/meta-poky/conf/distro/poky.conf || return
	if grep -q $DISTRIB_ID-$DISTRIB_RELEASE yocto/poky/meta-poky/conf/distro/poky.conf; then
		# assuming listed in SANITY_TESTED_DISTROS
		yocto-build-direct || return
	else
		# purge junk
		find yocto/build_bin -size 0 -a \( -name '*.o' -o -name '*.lo' \) -delete
		# assuming not listed in SANITY_TESTED_DISTROS, so build via docker
		echo "If docker fails with 'connect: permission denied.' please logout and login to activate usermod"
		_docker yocto-build-direct || return
		# USE_DOCKER=1 make # - doesn't work with outside symbolic links
	fi
	swicwe -P $fw
	test -f $fw # return status
}

kernel-menuconfig()
{
	pushd $sierra/yocto/
	BDIR=build_bin/
	. poky/oe-init-build-env # changes dir to BDIR
	bitbake -c listtasks linux-quic
	bitbake -c menuconfig linux-quic
	popd
}

all()
{
	prepare
	unset LD_LIBRARY_PATH
	source $toolchain/environment-setup-armv7a-neon-poky-linux-gnueabi
	expr match $- '.*x' > /dev/null && echo $PATH
	#make -C $LEGATO_ROOT clean
	make -C $LEGATO_ROOT -k
	make -C $LEGATO_ROOT -k framework_$target || return
	make -C $MANGOH_ROOT ${board}_$target LEGATO=1 # || return # calls make -C $LEGATO_ROOT -k framework_$target
	ls -l $MANGOH_ROOT/build/update_files/$board/mangOH.$target.update
	# JDK_INCLUDE_DIR sometimes makes problems
	# JDK_INCLUDE_DIR=/usr/lib/jvm/java-8-openjdk-amd64/include
	#make -C $LEGATO_ROOT -k tests_$target samples_$target
	#make -C $LEGATO_ROOT -k samples_$target
	make -C $LEGATO_ROOT -k all_$target # calls tests_$target and samles_$target
	yocto-build || return
	_status
}

clean()
{
	make -C $LEGATO_ROOT clean
	make -C $MANGOH_ROOT clean
	(cd yocto 2> /dev/null && make clean) # 'make -C yocto' fails
	rm legato yocto 2> /dev/null # remove non-recursive, only symlinks
}

docker-guest()
{
	ln -sf bash /bin/sh
	whoami
	LANG=en_US.UTF-8
	if ! grep -q $LANG /etc/default/locale; then
		apt-get --quiet update
		apt-get --quiet install -y sudo locales
		dpkg-reconfigure --frontend=noninteractive locales
		locale-gen $LANG
		update-locale LANG=$LANG
	fi
	local l="$USER ALL=(ALL) NOPASSWD: ALL"
	local f=/etc/sudoers
	grep --quiet --line-regexp --fixed-strings "$l" "$f" || echo "$l" >> "$f"
	su $USER -c ". config.sh; . sierra-tools.sh prepare"
	su $USER -c ". config.sh; . $0 $@"
}

_docker()
{
	dpkg -V docker.io || sudo apt-get install -y -q $_
	groups $USER | grep -q docker || sudo usermod $USER --append --groups docker
	showv release > config.sh
	local n=build${PWD//\//-}
	if ! docker inspect $n > /dev/null; then
		docker run \
			--name $n \
			--volume "$HOME:$HOME" \
			--interactive --tty \
			--hostname ${HOSTNAME} \
			--volume /etc/passwd:/etc/passwd \
			--volume /etc/group:/etc/group \
			--env USER=${USER} \
			--workdir ${PWD} \
			ubuntu:16.04 \
			bash -c "./sierra-tools.sh docker-guest $@"
	else
		docker start --attach --interactive $n
	fi
}

if [ -n "$*" ]; then
	config-env
	eval "$*" # execute arguments
	#echo $* finished, ret=$?
else
	if [ "$0" != "$BASH_SOURCE" ]; then
		echo $BASH_SOURCE functions are loaded into the shell environment
	else
		echo $BASH_SOURCE - a library of Sierra, mangOH, legato and yocto tools
		echo Try to run \"$BASH_SOURCE all\"
	fi
fi
